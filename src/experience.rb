# frozen_string_literal: true

def print_twice (a)
  puts a
  yield
  yield
end

print_twice "hello" do
  puts "fuck me"
end

# "Hello"
# "Hello"

class LinkedList
  def initialize(value, next_)
    @value = value
    @next = next_
  end

  def to_s
    @value.to_s
  end

  def to_str
    to_s
  end

  attr_accessor :value
  attr_accessor :next

  @next
end

ll = LinkedList.new 10, nil

puts "hello" + ll

def balanced? (s)
  counter = 0
  letters = s.split ""
  letters.each do | letter |
    if letter == "("
      counter += 1
    elsif letter == ")"
      counter -= 1
    end
    if counter < 0
      return false
    end
  end
  counter == 0
end

["()", "()()", "())", "()())"].each do |word|
  puts word + (balanced? word).to_s
end

(1...10).each do |x|
  puts x
end


def toto(&block)
  x =999
  y = 888
  z = block.yield 99, 88
  z + 1
end

ret = toto do |x, y|
  puts x + y
  45
end

puts "helo me"
puts "ret = #{ ret }"

puts "hello#world"

# foo =  toto: 89