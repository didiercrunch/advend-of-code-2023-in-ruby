# frozen_string_literal: true

class Card
  def initialize(id, selected_numbers, winning_numbers)
    @id = id
    @selected_numbers = selected_numbers
    @winning_numbers = winning_numbers
  end

  def matching_number
    @selected_numbers.filter {|s| @winning_numbers.index(s) != nil}.length
  end

  def score
    n = matching_number
    n == 0 ? 0 : 2 ** (n - 1)
  end

  def self.deserialize(line)
    id_match = /Card\s+(\d+)/.match(line)
    id = id_match[1].to_i
    winning_numbers = line[(line.index(":") + 1)..(line.index("|") - 1)].split(/\s+/).filter {|x| !x.empty?}.map {|x| x.to_i}
    selected_numbers = line[(line.index("|") + 1)..line.length].split(/\s+/).filter {|x| !x.empty?}.map {|x| x.to_i}
    Card.new id, selected_numbers, winning_numbers
  end

  attr_accessor :id
  attr_accessor :selected_numbers
  attr_accessor :winning_numbers
end


class Cards
  def initialize(cards)
    @cards = cards
    @scratchcards_generated_by_card_cache = {}
  end

  def self.deserialize(ser)
    cards = ser
              .split("\n")
              .filter {|line| !line.empty?}
              .map {|line| Card.deserialize line}
    self.new cards
  end

  def scores
    @cards.map {|card| card.score}
  end

  def scratchcards_generated_by_card(card)
    if @scratchcards_generated_by_card_cache.has_key? card.id
      return @scratchcards_generated_by_card_cache[card.id]
    end
    ret = [card]
    score = card.matching_number
    if score > 0
      sub_cards = @cards.filter {|sub_card| sub_card.id > card.id and sub_card.id <= card.id + score }
      ret += sub_cards
                  .map {|sub_card| scratchcards_generated_by_card(sub_card)}
                  .flatten
    end
    @scratchcards_generated_by_card_cache[card.id] = ret
    ret
  end

  def all_scratchcards
    ret = []
    @cards.each { |card| ret +=  scratchcards_generated_by_card card }
    ret

  end

  attr_accessor :cards

end

if __FILE__ == $0
  input_file_name = File.join(File.dirname(__FILE__), "day_4_input.txt")
  input_content = File.open(input_file_name).read
  cards = Cards.deserialize input_content

  puts "Solution to part 1: #{cards.scores.sum}"
  puts "Solution to part 2: #{cards.all_scratchcards.length}"
end
