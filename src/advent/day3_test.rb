# frozen_string_literal: true

require 'test/unit'
require_relative './day3'

class Day3Test < Test::Unit::TestCase
  def test_group_neighbour_empty
    ret = group_neighbour([])  {|x, y| x + 1 == y}
    assert_empty ret
  end

  def test_group_neighbour
    input = [3,5,6,7,1,2,4,1,1]
    ret = group_neighbour(input) do |x, y|
      y == x + 1
    end
    expt = [[3],[5,6,7],[1,2],[4],[1],[1]]
    assert_equal expt, ret
  end

  def test_experiments
    word = "hello"
    lst = [0, 1, 2]
    assert_equal lst[-1], 2

    assert_equal (0..2).to_a, [0, 1, 2]

    assert_true [1, 2, 3, 0].all?

    assert_equal (1..1).to_a, [1]

    assert_equal "hello"[4..10], "o"

    assert_true 0 <= 10 and 10 < 100

    assert_equal [1, 2] + [3, 4], [1,2,3,4]


    # assert_equal ((1..5) + (6..10)).to_a,  [1,2,3,4,5,6,7,8,9,10]

  end
end


class EngineLineTest < Test::Unit::TestCase

  @@easy_engine = "
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."
  def test_extract_numbers
    engine = EngineLine.new 33, ".664.598.."
    engine_numbers = engine.extract_engine_numbers
    assert_equal 2, engine_numbers.length
    assert_equal engine_numbers[0].start, 1
    assert_equal engine_numbers[0].line_index, 33
    assert_equal engine_numbers[0].end_, 1 + 3
    assert_equal engine_numbers[0].value, 664
  end

  def test_extract_engine_numbers
    engine = Engine.deserialize @@easy_engine
    expt = [467, 114, 35, 633, 617, 58, 592, 755, 664, 598]
    assert_equal expt, engine.extract_engine_numbers.map {|e| e.value}
  end

  def test_touches_symbol
    engine = Engine.deserialize @@easy_engine
    engine_number = engine.extract_engine_numbers[0]
    assert_true engine.touches_symbol? engine_number
  end

  def test_extract_engine_numbers_that_touches_symbols
    engine = Engine.deserialize @@easy_engine
    expt = [467, 35, 633, 617, 592, 755, 664, 598]
    assert_equal expt, engine.extract_engine_numbers_that_touches_symbols.map {|e| e.value}
  end

  def test_empty
    input = "
..........
..........
..........
..........
..........
..........
..........
..........
..........
.........."
    engine = Engine.deserialize input
    expt = []
    assert_equal expt, engine.extract_engine_numbers_that_touches_symbols.map {|e| e.value}
  end


  def test_day_3_part_1_easy
    engine = Engine.deserialize @@easy_engine
    expt = 4361
    assert_equal expt, engine.extract_engine_numbers_that_touches_symbols.map {|e| e.value}.sum
  end

  def test_extract_gears
    engine = Engine.deserialize @@easy_engine
    assert_equal 3, engine.extract_gears.length
  end

  def test_get_gear_ratios
    engine = Engine.deserialize @@easy_engine
    assert_equal [16345, 451490], engine.get_gear_ratios
  end

  def test_day_3_part_2_easy
    engine = Engine.deserialize @@easy_engine
    assert_equal 467835, engine.get_gear_ratios.sum
  end

end