# frozen_string_literal: true

require 'test/unit'
require_relative './day5'


class TestWeirMapRange < Test::Unit::TestCase
  def test_deserialize
    wmr = WeirMapRange.deserialize "37 52 2"

    assert_equal 37, wmr.destination
    assert_equal 52, wmr.source
    assert_equal 2, wmr.range_length
  end

  def test_happy_path
    wmr = WeirMapRange.deserialize "37 52 2"
    assert_false wmr.includes_destination? 36
    assert_true wmr.includes_destination? 37
    assert_true wmr.includes_destination? 38
    assert_false wmr.includes_destination? 39


    assert_false wmr.includes_source? 51
    assert_true wmr.includes_source? 52
    assert_true wmr.includes_source? 53
    assert_false wmr.includes_source? 54

    [51, 52, 53, 54].each do |source|
      assert_equal(source, wmr.source_for_destination(wmr.destination_for_source(source)))
    end

    [36,37,38,39].each do |destination|
      assert_equal(destination, wmr.destination_for_source(wmr.source_for_destination(destination)))
    end

  end
end


class TestWeirdMap < Test::Unit::TestCase
  def test_deserialize
    ser = "
50 98 2
52 50 48"
    wm = WeirdMap.deserialize ser
    assert_equal 0, wm[0]
    assert_equal 48, wm[48]
    assert_equal 49, wm[49]
    assert_equal 52, wm[50]
    assert_equal 53, wm[51]
    assert_equal 98, wm[96]
    assert_equal 99, wm[97]
    assert_equal 50, wm[98]
    assert_equal 51, wm[99]
  end
end

class TestTodoMap < Test::Unit::TestCase
  def test_deserialize
    ser = "
light-to-temperature map:
45 77 23
81 45 19
68 64 13"
    tm = TodoMap.deserialize ser
    assert_equal "light", tm.from
    assert_equal "temperature", tm.to
    assert_equal 86, tm[50]

  end
end


class TestPlanningMaps < Test::Unit::TestCase
  @@easy_input = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"

  def test_deserialization
    pms = PlanningMaps.deserialize @@easy_input
    assert_equal [79, 14, 55, 13], pms.seeds
  end

  def test_day_4_part_1
    pms = PlanningMaps.deserialize @@easy_input
    assert_equal 35, pms.min_destinations
  end
end

class TestPlanningMapsWithSeedRanges < Test::Unit::TestCase

  @@easy_input = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"

  def test_deserialization
    pms = PlanningMapsWithSeedRanges.deserialize @@easy_input
    expt = (79..92).to_a + (55..67).to_a
    assert_equal(expt, pms.seed_ranges.map {|r| r.to_a} .flatten)
  end

  def test_brute_force
    pms = PlanningMapsWithSeedRanges.deserialize @@easy_input
    assert_equal 46, pms.min_destinations_brute_force
    assert_equal( 82, pms.seed_of(46))
  end

  def test_day_5_part_2
    pms = PlanningMapsWithSeedRanges.deserialize @@easy_input
    assert_equal 46, pms.min_destinations
  end


end