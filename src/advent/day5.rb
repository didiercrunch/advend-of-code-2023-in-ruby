# frozen_string_literal: true

class WeirMapRange
  def initialize(destination, source, range_length)
    @destination = destination
    @source = source
    @range_length = range_length
  end

  def includes_source?(source)
    @source <= source and source < (@source + @range_length)
  end

  def includes_destination?(destination)
    @destination <= destination and destination < (@destination + @range_length)
  end

  def source_for_destination(destination)
    destination + @source - @destination
  end

  def destination_for_source(source)
    @destination + (source - @source)
  end

  def critical_points
    [@source - 1, @source, @source +  @range_length,  @source +  @range_length + 1]
  end

  def self.deserialize(ser)
    parts = ser.strip.split(/\s+/).map {|p| p.to_i}
    self.new parts[0], parts[1], parts[2]
  end

  attr_accessor :destination
  attr_accessor :source
  attr_accessor :range_length
end

class WeirdMap
  def initialize(weird_map_ranges)
    @weird_map_ranges = weird_map_ranges
  end

  def preimage_of(destination)
    @weird_map_ranges.each do |wmr|
      if wmr.includes_destination? destination
        return wmr.source_for_destination destination
      end
    end
    destination
  end

  def [](source)
    @weird_map_ranges.each do |wmr|
      if wmr.includes_source? source
        return wmr.destination_for_source source
      end
    end
    source
  end

  def self.deserialize(ser)
    wmr = ser.strip.split("\n")
       .filter {|line| !line.empty?}
       .map {|line| WeirMapRange.deserialize line}
    self.new wmr
  end

  def critical_points
    @weird_map_ranges.map {|r| r.critical_points} .flatten
  end


end


class TodoMap
  def initialize(from, to, hash)
    @from = from
    @to = to
    @hash = hash
  end

  def self.deserialize(ser)
    ser = ser.strip
    from_to =ser[(0...ser.index(/\s+/))].split("-")
    weird_map_ser = ser[ser.index("\n")..ser.length]
    self.new from_to[0], from_to[2], WeirdMap.deserialize(weird_map_ser)
  end

  def [](source)
    @hash[source]
  end

  def critical_points
    @hash.critical_points
  end

  def preimage_of(value)
    @hash.preimage_of value
  end

  attr_accessor :from
  attr_accessor :to
  attr_accessor :hash
end

class PlanningMaps
  def initialize(seeds, toto_maps)
    @seeds = seeds
    @toto_maps = toto_maps
  end

  def [](source)
    ret = source
    @toto_maps.each {|tm| ret = tm[ret]}
    ret
  end

  def self.deserialize(ser)
    blocks = ser.strip.split(/^$/)
    seeds = blocks[0].split(" ")[(1..)].map {|s| s.to_i}
    toto_maps = blocks[(1..)].map {|b| TodoMap.deserialize b}
    self.new seeds, toto_maps
  end

  def min_destinations
    @seeds.map {|seed| self[seed]}
          .min
  end

  attr_accessor :seeds
end

class PlanningMapsWithSeedRanges
  def initialize(seed_ranges, toto_maps)
    @seed_ranges = seed_ranges
    @toto_maps = toto_maps
  end

  def [](source)
    ret = source
    @toto_maps.each {|tm| ret = tm[ret]}
    ret
  end

  def self.deserialize_seed_ranges(ser_arr)
    ser_arr.each_slice(2).map {|start, length| (start...(start + length))}
  end

  def self.deserialize(ser)
    blocks = ser.strip.split(/^$/)
    seeds = blocks[0].split(" ")[(1..)].map {|s| s.to_i}
    toto_maps = blocks[(1..)].map {|b| TodoMap.deserialize b}
    self.new(self.deserialize_seed_ranges(seeds), toto_maps)
  end

  def _preimage_of(value, weird_maps)
    ret = value
    weird_maps.each do |weird_map|
      ret = weird_map.preimage_of ret
    end
    ret
  end

  def _critical_points_of(idx)
    @toto_maps[idx].critical_points.map {|p| _preimage_of(p, @toto_maps[0...idx])}
  end

  def critical_points
    (0...@toto_maps.length).map {|idx| _critical_points_of idx}.flatten.uniq.sort
  end

  def critical_points_in_range(seed_range)
    underlying_critical_points = critical_points
    (underlying_critical_points + [seed_range.min, seed_range.max]).filter {|p| seed_range.include? p}
  end

  def min_of_seed_range(seed_range)
    cpts = critical_points_in_range(seed_range)
    cpts.map {|s| self[s]}.min
  end

  def all_seeds
    @seed_ranges.map {|r| r.to_a}.flatten
  end

  def min_destinations
    @seed_ranges.map  {|seed_range| min_of_seed_range seed_range}.min
  end

  def min_destinations_brute_force
    all_seeds.map {|s| self[s]}.min
  end

  def seed_of(destination)
    all_seeds.each do |s|
      if self[s] == destination
        return s
      end
    end
  end

  attr_accessor :seed_ranges
end


if __FILE__ == $0
  input_file_name = File.join(File.dirname(__FILE__), "day_5_input.txt")
  input_content = File.open(input_file_name).read
  pms = PlanningMaps.deserialize input_content
  pmswsr = PlanningMapsWithSeedRanges.deserialize input_content

  puts "Solution to part 1: #{pms.min_destinations}"
  puts "Solution to part 2: #{pmswsr.min_destinations}"

end
