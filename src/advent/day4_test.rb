# frozen_string_literal: true

require 'test/unit'
require_relative './day4'


class TestCard < Test::Unit::TestCase
  def test_deserialize
    line = "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"
    ret = Card.deserialize(line)
    assert_equal 3, ret.id
    assert_equal [69, 82, 63, 72, 16, 21, 14,  1], ret.selected_numbers
    assert_equal [1, 21, 53, 59, 44], ret.winning_numbers
  end

  def test_score
    line = "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"
    card = Card.deserialize(line)
    assert_equal 2,  card.score
  end

  def test_score2
    line = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
    card = Card.deserialize(line)
    assert_equal 8,  card.score
  end
  def test_score_bug
    line = "Card\t1:\t41\t48\t83 86 17\t|\t83 86\t6 31 17  9 48 53"
    card = Card.deserialize(line)
    assert_equal 8,  card.score
  end
end

class TestCards < Test::Unit::TestCase

  @@easy_input = "
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"

  def test_deserialize
    cards = Cards.deserialize @@easy_input
    assert_equal 6, cards.cards.length
  end

  def test_day_4_part_1
    cards = Cards.deserialize @@easy_input
    assert_equal 13, cards.scores.sum
  end

  def test_scratchcards_generated_by_card
    cards = Cards.deserialize @@easy_input
    ret = cards.scratchcards_generated_by_card cards.cards[0]
    expt = [1, 2, 3, 4, 5, 5, 4, 5, 3, 4, 5, 5, 4, 5, 5]
    assert_equal expt, ret.map {|card| card.id}
  end

  def counter(lst)
    ret = {}
    lst.each do |e|
      if ret.has_key? e
        ret[e] += 1
      else
        ret[e] = 1
      end
    end
    ret
  end

  def test_day_4_part_2
    cards = Cards.deserialize @@easy_input
    assert_equal( 30, cards.all_scratchcards.length)
  end
end

#Once all of the originals and copies have been processed, you end up with
# 1 instance of card 1
# , 2 instances of card 2,
# 4 instances of card 3,
# 8 instances of card 4,
# 14 instances of card 5,
# and 1 instance of card 6.
#
#
# In total, this example pile of scratchcards causes you to ultimately have 30 scratchcards!