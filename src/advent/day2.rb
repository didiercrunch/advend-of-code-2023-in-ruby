# frozen_string_literal: true

class Draw
  def initialize(blue, red, green)
    @blue = blue
    @red = red
    @green = green
  end

  def can_be_played_with(blue, red, green)
    # puts "blue #{@blue} <= #{ blue } | red #{@red} <= #{ red } |  green #{@green} <= #{ green }"
    @blue <= blue and @red <= red and @green <= green
  end

  def self.deserialize(ser)
    parts = ser.strip.split(",")
    vals = {}
    parts.each do |part|
      match = /^\s*(\d+)\s+(\w+)\s*$/.match(part)
      vals[match[2]] = match[1].to_i
    end
    self.new(vals["blue"] || 0, vals["red"] || 0, vals["green"] || 0)
  end

  attr_accessor :red
  attr_accessor :green
  attr_accessor :blue

end

class Game
  def initialize(id, draws)
    @id = id
    @draws = draws
  end

  def can_be_played_with(blue, red, green)
    @draws.all? do |draw|
      draw.can_be_played_with blue, red, green
    end
  end

  def self.deserialize(line)
    id = /\s*Game\s+(\d+):/.match(line)[1].to_i
    draws_as_text = line[line.index(":") + 1..]
    draws = draws_as_text.split(";").map(&Draw.method(:deserialize))
    self.new id, draws
  end

  def fewer_number_of_cubes
    [:blue, :red, :green].map do |color|
      @draws.map {|draw| draw.send color }
            .max || 0
    end
  end

  def power_of_the_fewer_number_of_cubes
    fewer_number_of_cubes.reduce 1, :*
  end

  attr_accessor :id
end

easy_inputs = "
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"

# one_game = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
# game = Game.deserialize one_game
# puts game.power_of_the_fewer_number_of_cubes
# return

input_file_name = File.join(File.dirname(__FILE__), "day_2_input.txt")

input_content = File.open(input_file_name).read

answer_1 = input_content.split("\n")
           .filter {|line| ! line.empty?}
           .map(& Game.method(:deserialize))
           .filter {|game| game.can_be_played_with(14, 12, 13)}
           .map {|game| game.id}
           .sum
puts "answer_1: #{answer_1}"

answer_2 = input_content.split("\n")
                        .filter {|line| ! line.empty?}
                        .map(& Game.method(:deserialize))
                        .map {|game| game.power_of_the_fewer_number_of_cubes}
                        .sum
puts "answer_2: #{answer_2}"
# puts "9".to_i()
# dict = {}
# dict["one"] = 34
# dict["two"] = 90
# puts dict["dd"] || 90
