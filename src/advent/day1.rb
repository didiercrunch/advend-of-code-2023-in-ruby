# frozen_string_literal: true

DIGIT_NAMES = {one: "1", two: "2", three: "3", four: "4" ,
               five: "5", six: "6", seven: "7", eight: "8",
               nine: "9"}
def extracts_digits (word, ret=[])
  if word == nil || word.empty?
    return ret
  end
  if word.start_with? /\d/
    ret.append word[0]
    return extracts_digits word[1..], ret
  end

  DIGIT_NAMES.each do |name, value|
    if word.start_with? name.to_s
      ret.append  value
      return extracts_digits word[1..], ret
    end
  end
  extracts_digits word[1..], ret
end


def fist_letter(line)
  extracts_digits(line)[0]
end

def last_digit(line)
  extracts_digits(line)[-1]
end

def get_calibration_code(line)
  if line.empty?
    0
  else
    (fist_letter(line) + last_digit(line)).to_i
  end
end

def get_sum_of_calibration_code(document)
  calibration_codes = document.split("\n").map do |line|
    get_calibration_code line
  end
  calibration_codes.reduce 0, :+
end

# puts "on".length

# puts extracts_digits "7pqrstsixteen"
mock_input = "
8nine37bpkmtghhnc2hnreightwohvs
"

input_file_name = File.join(File.dirname(__FILE__), "day_1_input.txt")

input_content = File.open(input_file_name).read
puts get_sum_of_calibration_code(input_content)
# puts extracts_digits mock_input