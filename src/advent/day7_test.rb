# frozen_string_literal: true
require_relative './day7'
require 'test/unit'

class TestHand < Test::Unit::TestCase

  def test_get_five_of_a_kind
    assert_equal Hand.new("22222", 0).get_type, HandType.new(HandTypeName::FIVE_OF_A_KIND, "2")
  end

  def test_get_four_of_a_kind
    assert_equal Hand.new("56555", 0).get_type, HandType.new(HandTypeName::FOUR_OF_A_KIND, "5")
  end

  def test_get_full_house
    assert_equal Hand.new("56565", 0).get_type, HandType.new(HandTypeName::FULL_HOUSE, "56")
  end

  def test_get_three_of_a_kind
    assert_equal Hand.new("56575", 0).get_type, HandType.new(HandTypeName::THREE_OF_A_KIND, "5")
  end

  def test_get_two_pairs
    assert_equal Hand.new("56568", 0).get_type, HandType.new(HandTypeName::TWO_PAIR, "56")
  end

  def test_get_one_pair
    assert_equal Hand.new("56543", 0).get_type, HandType.new(HandTypeName::ONE_PAIR, "5")
  end

  def test_high_card
    assert_equal Hand.new("26543", 0).get_type, HandType.new(HandTypeName::HIGH_CARD, "6")
    assert_equal Hand.new("2K5Q3", 0).get_type, HandType.new(HandTypeName::HIGH_CARD, "K")
  end

end


class TestPart1 < Test::Unit::TestCase
  def test_part_1
    easy_input = "
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"
    ret = play_part_1_game easy_input
    assert_equal 6440, ret
  end
end