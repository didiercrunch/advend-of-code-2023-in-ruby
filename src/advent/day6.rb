# frozen_string_literal: true

def get_critical_points(time, record)
  delta = Math.sqrt(time*time - 4 * record)
  epsilon = 0.000000000001 # math is not life
  [(time - delta)/2, (time + delta)/2 - epsilon]
end

def number_of_possible_way_to_beat_the_record(time, record)
  critical_points = get_critical_points(time, record)
  critical_points[1].floor - critical_points[0].floor
end


inputs = [[56, 334], [71, 1135], [79, 1350], [99, 2430]]
part_1 = inputs.map { |x| number_of_possible_way_to_beat_the_record x[0], x[1] }
      .reduce 1, :*

puts "Part 1: #{part_1}"


part_2 = number_of_possible_way_to_beat_the_record 56717999,  334113513502430

puts "Part 2: #{part_2}"


